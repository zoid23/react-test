import '../src/index.css';
import { addDecorator, configure } from '@storybook/react';
import { setIntlConfig, withIntl } from 'storybook-addon-intl';
import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import messages from '../src/translation'
 
addLocaleData(enLocaleData);
 
const getMessages = (locale) => messages[locale];
 
setIntlConfig({
    locales: ['en-US'],
    defaultLocale: 'en-US',
    getMessages
});

addDecorator(withIntl);

const req = require.context('../src/', true, /story\.js$/)

function loadStories() {
  req.keys().forEach(req)
}

configure(loadStories, module);