import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { Login } from './ui/pages/Login'
import LocaleProvider from './LocaleProvider'
import { Home } from './ui/pages/Home'
import Loading from './ui/pages/Loading';

class App extends Component {
  render = () => (
    <LocaleProvider>
      <Route path="/login" component={Login} />
      <Route path="/home" component={Home} />
      <Route exact path="/" render={() => (<Redirect to="/home" />)} />
      <Loading />
    </LocaleProvider>
  )
}

export default App