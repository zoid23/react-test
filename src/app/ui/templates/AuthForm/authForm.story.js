import React from 'react';
import { storiesOf } from '@storybook/react';
import AuthForm from '.';

storiesOf('AuthForm', module)
  .add('login', () => (
    <div>
      <AuthForm
        validateEmail={email => email === 'luigi@wink.by'}
        validatePassword={pwd => pwd === 'qweasd'}
        submitMessage="Login"
        onSubmit={(email, pwd) => alert(`${email} ${pwd}`)} />
        <p>Login credentials luigi@wink.by qweasd</p>
    </div>
  ))
  .add('register', () => (
    <AuthForm
      submitMessage="Register"
      onSubmit={(email, pwd) => alert(`${email} ${pwd}`)} />
  ))
  .add('with error', () => (
    <AuthForm
      submitMessage="Register"
      onSubmit={(email, pwd) => alert(`${email} ${pwd}`)}
      errorMessage="User already exist" />
  ))