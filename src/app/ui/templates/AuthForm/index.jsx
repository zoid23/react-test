import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import ListContainer from '../../atoms/ListContainer'
import EmailInputField from '../../organisms/EmailInputField'
import PasswordInputField from '../../organisms/PasswordInputField'
import Button from '../../atoms/Button'
import styled from 'styled-components'
import Message from '../../atoms/Message';

const RightButton = styled(Button)`
  float: right;
`

class AuthForm extends Component {

  state = {}

  render() {
    const {
      email,
      password,
      submitMessage,
      passwordMinLength,
      validatePassword,
      validateEmail,
      intl,
      errorMessage
    } = this.props

    const {
      emailIsValid,
      passwordIsValid
    } = this.state

    return (
      <ListContainer>
        <EmailInputField
          title={intl.formatMessage({ id: 'auth.email' })}
          value={email}
          validate={validateEmail}
          onChange={this.onEmailChange.bind(this)}
        />
        <PasswordInputField
          title={intl.formatMessage({ id: 'auth.password' })}
          value={password}
          validate={validatePassword}
          minLength={passwordMinLength}
          onChange={this.onPasswordChange.bind(this)} />
        <div>
          <RightButton
            disabled={!emailIsValid || !passwordIsValid}
            onClick={this.onSubmit.bind(this)}
          >
            {submitMessage}
          </RightButton>
        </div>
        { errorMessage && <Message error>{errorMessage}</Message>}
      </ListContainer>
    )
  }

  onEmailChange = (currentEmail, emailIsValid) => this.setState({
    currentEmail,
    emailIsValid
  })

  onPasswordChange = (currentPassword, passwordIsValid) => this.setState({
    currentPassword,
    passwordIsValid
  })

  onSubmit = () => this.props.onSubmit(
    this.state.currentEmail,
    this.state.currentPassword
  )

}

AuthForm.propTypes = {
  email: PropTypes.string,
  password: PropTypes.string,
  validateEmail: PropTypes.func,
  validatePassword: PropTypes.func,
  submitMessage: PropTypes.string,
  passwordMinLength: PropTypes.number,
  errorMessage: PropTypes.string,
  onSubmit: PropTypes.func
}

AuthForm.defaultProps = {
  email: '',
  password: '',
  passwordMinLength: 6,
  validateEmail: () => true,
  validatePassword: () => true,
}

export default injectIntl(AuthForm)