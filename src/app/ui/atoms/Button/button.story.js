import React from 'react';
import { storiesOf } from '@storybook/react';
import Button from '.';

storiesOf('Button', module)
  .add('primary', () => (
    <Button
      onClick={() => console.log('click')}
    >
      Primary button
    </Button>
  ))
  .add('disabled', () => (
    <Button
      onClick={() => console.log('click')}
      disabled
    >
      Primary button
    </Button>
  ))