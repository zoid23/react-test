import styled, { css } from 'styled-components'

export default styled.div`
  border: 0;
  border-radius: 4px;
  background: var(--primary-color);
  padding: 0.6em 1.2em;
  font-size: 0.8em;
  margin: 0.5em 0px;
  color: white;
  font-weight: bold;
  text-transform: uppercase;
  ${props =>
    props.disabled &&
    css`  
      background: var(--primary-divider-color);
    `};
`
