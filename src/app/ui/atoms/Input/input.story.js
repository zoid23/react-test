import React from 'react';
import { storiesOf } from '@storybook/react';
import Input from '.';

storiesOf('Input', module)
  .add('primary', () => (
    <Input />
  ))
  .add('error', () => (
    <Input error />
  ))
  .add('placeholder', () => (
    <Input error placeholder="Hint text"/>
  ))