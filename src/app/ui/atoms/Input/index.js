import styled, { css } from 'styled-components'

export default styled.input`
  border: 0;
  outline: 0;
  background: transparent;
  border-bottom: 1px solid var(--primary-divider-color);
  font-size: 1em;
  padding: 0.5em 0px;
  width: 100%;
  ${props =>
    props.error &&
    css`
      border-bottom: 2px solid var(--primary-error-color);
    `};
`