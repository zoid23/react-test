import React from 'react';
import { storiesOf } from '@storybook/react';
import Message from '.';

storiesOf('Message', module)
  .add('primary', () => (
    <Message>General message</Message>
  ))
  .add('error', () => (
    <Message error>Error message</Message>
  ))