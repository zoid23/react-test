import styled, { css } from 'styled-components'

export default styled.div`
  border: 1px solid var(--primary-divider-color);
  border-radius: 4px;
  padding: 0.6em 1.2em;
  font-size: 0.8em;
  margin: 1em 0em;
  color: var(--primary-divider-color);
  font-weight: 100;
  ${props =>
    props.error &&
    css`  
      border: 1px solid var(--primary-error-color);
      color: var(--primary-error-color);
    `};
`