import styled from 'styled-components'

export default styled.button`
  border-radius: 50%;
  animation: spin 1s linear infinite;
  margin: auto;
  left: 0;
  right: 0;
  width: 60px;
  height: 60px;
  border: 8px solid transparent;
  border-top: 8px solid var(--primary-color);

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
`