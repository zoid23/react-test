import styled from 'styled-components'

export default styled.h3`
  color: var(--primary-text-color);
  font-size: 1.5em;
`