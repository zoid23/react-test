import React from 'react';
import { storiesOf } from '@storybook/react';
import Title from '.';

storiesOf('Title', module)
  .add('primary', () => (
    <Title>Primary Title</Title>
  ))