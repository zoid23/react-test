import React from 'react';
import { storiesOf } from '@storybook/react';
import MainContainer from '.';

storiesOf('MainContainer', module)
  .add('primary', () => (
    <MainContainer><p>Paragraph</p></MainContainer>
  ))