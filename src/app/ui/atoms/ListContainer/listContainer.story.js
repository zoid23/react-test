import React from 'react';
import { storiesOf } from '@storybook/react';
import ListContainer from '.';

storiesOf('ListContainer', module)
  .add('simple list', () => (
    <ListContainer>
      <p>Paragraph</p>
      <p>Paragraph2</p>
      <p>Paragraph3</p>
      <p>Paragraph4</p>
    </ListContainer>
  ))