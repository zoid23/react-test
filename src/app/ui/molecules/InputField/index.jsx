import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Input from '../../atoms/Input'
import ListContainer from '../../atoms/ListContainer'

const Container = styled(ListContainer)`
  margin: 10px 0px;
`
const Message = styled.a`
  font-size: 0.7em;
  color: var(--secondary-text-color);
  font-weight: bold;
`

const ErrorMessage = styled(Message)`
  padding: 0.2em 0.5em;
  color: var(--primary-error-color);
  font-weight: normal;
`

class InputField extends Component {
  render() {
    const { message, errorMessage, ...inputProps } = this.props
    
    return (
      <Container>
        <Message>{message}</Message>
        <Input error={!!errorMessage} {...inputProps} />
        <ErrorMessage>{errorMessage}</ErrorMessage>
      </Container>
    )
  }
}

InputField.propTypes = {
  message: PropTypes.string,
  errorMessage: PropTypes.string,
}

export default InputField