import React from 'react';
import { storiesOf } from '@storybook/react';
import InputField from '.';

storiesOf('InputField', module)
  .add('primary', () => (
    <InputField message="default message" />
  ))
  .add('error', () => (
    <InputField errorMessage="wrong value" message="default message" />
  ))