import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import Progress from '../../atoms/Progress'
import PropTypes from 'prop-types'

const ProgressContainer = styled.div`
  background: rgba(255, 255, 255, 0.7);
  display: flex;
  justify-content: center;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  ${props =>
    !props.visible &&
    css`  
      visibility: hidden
    `};
`

class ProgressOverlay extends Component {
  render = () => (
    <ProgressContainer {...this.props}>
      <Progress />
    </ProgressContainer>
  )
}

ProgressOverlay.propTypes = {
  visible: PropTypes.bool
}

export default ProgressOverlay