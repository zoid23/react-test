import React from 'react';
import { storiesOf } from '@storybook/react';
import ProgressOverlay from '.';

storiesOf('ProgressOverlay', module)
  .add('visible', () => (
    <ProgressOverlay visible />
  ))
  .add('invisible', () => (
    <ProgressOverlay visible={false} />
  ))