import React, { Component } from 'react'
import MainContainer from '../../atoms/MainContainer'
import Title from '../../atoms/Title'
import { connect } from 'react-redux'
import { injectIntl, FormattedMessage } from 'react-intl'
import ListContainer from '../../atoms/ListContainer'
import { getUser } from '../../../store/user/user.selectors'
import { Redirect } from 'react-router-dom'

class Home extends Component {
  render() {
    const { authenticated, userEmail } = this.props

    if (!authenticated) 
      return (<Redirect to="/login"/>)

    return (
      <MainContainer>
        <ListContainer>
          <Title><FormattedMessage id="home.title" /></Title>
          <div><FormattedMessage id="home.welcome" values={{ email: userEmail }} /></div>
        </ListContainer>
      </MainContainer>
    )
  }
}

const mapStateToProps = state => ({
  authenticated: getUser(state).authenticated,
  userEmail: getUser(state).email
})

export default connect(
  mapStateToProps,
  null
)(injectIntl(Home))