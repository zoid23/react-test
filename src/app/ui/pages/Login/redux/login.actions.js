import { decoreAction } from '../../../../store/decorators'

export const clean = decoreAction(() => ({
  type: 'CLEAN'
}))

export const setErrorId = decoreAction(messageId => ({
  type: 'SET_ERROR_ID',
  payload: messageId
}))