import * as userActions from '../../../../store/user/user.actions'
import * as actions from './login.actions'
import { setVisibility as setIsLoading } from '../../Loading/redux/loading.actions'
import { put, takeLatest } from 'redux-saga/effects'

export const onSetUserAuthenticated =  function* (action) {
  const { authenticated } = action.payload
  yield put(setIsLoading(false))

  if (!authenticated)
    yield put(actions.setErrorId('auth.loginerror'))
}

export const onSetUser =  function* () {
  yield put(setIsLoading(false))
}

export const onAuthenticate = function* () {
  yield put(setIsLoading(true))
  yield put(actions.setErrorId(undefined))
}

export default function* () {
  yield takeLatest(userActions.setUserAuthenticated.type, onSetUserAuthenticated)
  yield takeLatest(userActions.setUser.type, onSetUser)
  yield takeLatest(userActions.authenticateUser.type, onAuthenticate)
}