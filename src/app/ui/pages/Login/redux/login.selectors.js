import get from 'lodash/get'
import reducers from './login.reducers'
import { componentsSelector } from '../../../../store/components/components.selectors'

export const getErrorMessage = componentsSelector(state => get(state, `${reducers.STATE_KEY}.messageId`))