import { handleActions } from 'redux-actions'
import * as actions from './login.actions'

export const DEFAULT_STATE = {}

export const setErrorMessage = (state, { payload }) => {
  return {
    ...state,
    messageId: payload
  }
}

export const clean = () => DEFAULT_STATE

const reducers = handleActions(
  {
    [actions.setErrorId.type]: setErrorMessage
  },
  DEFAULT_STATE
)

export default {
  STATE_KEY: 'login',
  reducers
}