import React, { Component } from 'react'
import MainContainer from '../../atoms/MainContainer'
import Title from '../../atoms/Title'
import { connect } from 'react-redux'
import { injectIntl, FormattedMessage } from 'react-intl'
import AuthForm from '../../templates/AuthForm';
import ListContainer from '../../atoms/ListContainer';
import { getUser } from '../../../store/user/user.selectors'
import { getErrorMessage } from './redux/login.selectors'
import { authenticateUser } from '../../../store/user/user.actions'
import { clean } from './redux/login.actions'
import { Redirect } from 'react-router-dom'

class Login extends Component {
  render() {
    const { intl, errorMessageId, authenticate, alreadySigned, authenticated } = this.props

    const errorMessage = errorMessageId && intl.formatMessage({ id: errorMessageId })

    if (authenticated) 
      return (<Redirect to="/home"/>)

    return (
      <MainContainer>
        <ListContainer>
          <Title><FormattedMessage id="auth.login" /></Title>
          <AuthForm
            errorMessage={errorMessage}
            submitMessage={intl.formatMessage({
              id: alreadySigned ? 'auth.login':'auth.register'
            })}
            onSubmit={authenticate}
          />
        </ListContainer>
      </MainContainer>
    )
  }

  componentWillUnmount = () => this.props.clean()
}



const mapStateToProps = state => ({
  authenticated: getUser(state).authenticated,
  errorMessageId: getErrorMessage(state),
  alreadySigned: getUser(state).email
})

const mapDispatchToProps = dispatch => ({
  authenticate: (email, password) => dispatch(authenticateUser(email, password)),
  clean: () => dispatch(clean())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(Login))