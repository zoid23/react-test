import React, { Component } from 'react'
import { connect } from 'react-redux'
import ProgressOverlay from '../../molecules/ProgressOverlay';
import { getVisibility } from './redux/loading.selectors'

class Loading extends Component {
  render = () => (
    <ProgressOverlay visible={this.props.visible} />
  )
}

const mapStateToProps = state => ({
  visible: getVisibility(state)
})

export default connect(
  mapStateToProps,
  null
)(Loading)