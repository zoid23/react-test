import { decoreAction } from '../../../../store/decorators'

export const setVisibility = decoreAction(visible => ({
  type: 'SET_LOADING_VISIBILITY',
  payload: visible
}))