import get from 'lodash/get'
import reducers from './loading.reducers'
import { componentsSelector } from '../../../../store/components/components.selectors'

export const getVisibility = componentsSelector(state => get(state, `${reducers.STATE_KEY}.visible`))