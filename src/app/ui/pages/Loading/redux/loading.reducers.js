import { handleActions } from 'redux-actions'
import * as actions from './loading.actions'

export const DEFAULT_STATE = {}

export const setVisibility = (state, { payload }) => ({
  visible: payload
})

const reducers = handleActions(
  {
    [actions.setVisibility.type]: setVisibility
  },
  DEFAULT_STATE
)

export default {
  STATE_KEY: 'loading',
  reducers
}