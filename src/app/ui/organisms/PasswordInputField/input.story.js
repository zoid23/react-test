import React from 'react';
import { storiesOf } from '@storybook/react';
import PasswordInputField from '.';

storiesOf('PasswordInputField', module)
  .add('primary', () => (
    <PasswordInputField
      validate={pwd => pwd === 'qweasd'}
      onChange={(pwd, valid) => console.log(pwd, valid)} />
  ))
  .add('with title', () => (
    <PasswordInputField 
      title="Insert qweasd as password"
      validate={pwd => pwd === 'qweasd'}
      onChange={(pwd, valid) => console.log(pwd, valid)} />
  ))
  .add('with title and value', () => (
    <PasswordInputField 
      value="qweasd"
      title="Insert qweasd as password"
      validate={pwd => pwd === 'qweasd'}
      onChange={(pwd, valid) => console.log(pwd, valid)} />
  ))
  .add('error', () => (
    <PasswordInputField 
      value="12345"
      title="Insert qweasd as password"
      validate={pwd => pwd === 'qweasd'}
      onChange={(pwd, valid) => console.log(pwd, valid)} />
  ))

  .add('minLength', () => (
    <PasswordInputField 
      title="Insert 8 characters as password"
      validate={() => true}
      minLength={8}
      onChange={(pwd, valid) => console.log(pwd, valid)} />
  ))