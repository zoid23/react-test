import React, { Component } from 'react'
import InputField from '../../molecules/InputField'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'

class PasswordInputField extends Component {

  state = {}

  constructor(props) {
    super(props)
    this.state.currentPwd = props.value
  }

  componentWillMount = () => this.onChange(this.props.value)

  render() {
    const { state: { currentPwd }, props: { minLength, title, intl } } = this

    const error = (this.haveError() && intl.formatMessage({ id: 'shared.wrong_password' })) || undefined

    return (
      <InputField
        value={currentPwd}
        errorMessage={error}
        placeholder={intl.formatMessage({ id: 'shared.password' }, { minLength })}
        message={title}
        type="password"
        onChange={e => this.onChange(e.target.value)} />
    )
  }

  isValid = (value, minLength) => value.length >= minLength && this.props.validate(value)

  onChange = value => this.setState({
    currentPwd: value,
    isValid: this.isValid(value, this.props.minLength)
  }, () => {
    const { currentPwd, isValid } = this.state
    this.props.onChange(currentPwd, isValid)
  })

  haveError() {
    const { currentPwd, isValid } = this.state
    return currentPwd && !isValid
  }
}

PasswordInputField.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  validate: PropTypes.func,
  minLength: PropTypes.number,
  value: PropTypes.string
}

PasswordInputField.defaultProps = {
  minLength: 6,
  value: '',
  onChange: () => { }
}

export default injectIntl(PasswordInputField)