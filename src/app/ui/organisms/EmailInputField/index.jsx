import React, { Component } from 'react'
import InputField from '../../molecules/InputField'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'

const validateEmail = email => {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

class EmailInputField extends Component {

  state = {}

  constructor(props) {
    super(props)
    this.state.currentEmail = props.value
  }

  componentWillMount = () => this.onChange(this.props.value)

  render() {
    const { state: { currentEmail }, props: { title, intl } } = this

    const error = (this.haveError() && intl.formatMessage({ id: 'shared.invalid_email' })) || undefined

    return (
      <InputField
        value={currentEmail}
        errorMessage={error}
        placeholder={intl.formatMessage({ id: 'shared.email' })}
        message={title}
        onChange={e => this.onChange(e.target.value)} />
    )
  }

  onChange = value => this.setState({
    currentEmail: value,
    isValid: validateEmail(value) && this.props.validate(value)
  }, () => {
    const { currentEmail, isValid } = this.state
    this.props.onChange(currentEmail, isValid)
  })

  haveError() {
    const { currentEmail, isValid } = this.state
    return currentEmail && !isValid
  }
}

EmailInputField.propTypes = {
  validate: PropTypes.func,
  title: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string
}

EmailInputField.defaultProps = {
  title: '',
  value: '',
  onChange: () => {},
  validate: () => true
}

export default injectIntl(EmailInputField)