import React from 'react';
import { storiesOf } from '@storybook/react';
import EmailInputField from '.';

storiesOf('EmailInputField', module)
  .add('primary', () => (
    <EmailInputField
      onChange={(email, valid) => console.log(email, valid)} />
  ))
  .add('with title', () => (
    <EmailInputField 
      title="Insert a valid email for your account"
      onChange={(email, valid) => console.log(email, valid)} />
  ))
  .add('with title and value', () => (
    <EmailInputField 
      value="info@test.com"
      title="Insert a valid email for your account"
      onChange={(email, valid) => console.log(email, valid)} />
  ))
  .add('error', () => (
    <EmailInputField 
      value="info@test,com"
      title="invalid email case"
      onChange={(email, valid) => console.log(email, valid)} />
  ))