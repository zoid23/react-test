import React from 'react';
import { storiesOf } from '@storybook/react';
import Footer from '.';

storiesOf('Footer', module)
  .add('primary', () => (
    <Footer 
      items={['Item 1', 'Item 2']}
      onSelect={item => console.log(item)}/>
  ))