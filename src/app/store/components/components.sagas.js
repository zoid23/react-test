import { fork, all } from 'redux-saga/effects'
import loginSagas from '../../ui/pages/Login/redux/login.sagas'

const sagas = [
  loginSagas
]

export default function* () {
  yield all(sagas.map(saga => fork(saga)))
}