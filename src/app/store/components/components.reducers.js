import { combineReducers } from 'redux'
import transform from 'lodash/transform'
import set from 'lodash/set'
import loginReducers from '../../ui/pages/Login/redux/login.reducers'
import loadingReducers from '../../ui/pages/Loading/redux/loading.reducers'

const all = [
  loadingReducers,
  loginReducers
]

const reducers = transform(all, (reducers, value) => set(reducers, value.STATE_KEY, value.reducers), {})

export default {
  STATE_KEY: 'components',
  reducers: combineReducers(reducers)
}