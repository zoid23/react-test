import get from 'lodash/get'
import reducers from './components.reducers'

export const componentsSelector = selector => state => selector(
  get(state, `${reducers.STATE_KEY}`)
)