import { fork, all } from 'redux-saga/effects'
import userSagas from './user/user.sagas'
import componentsSagas from './components/components.sagas'

const sagas = [
  componentsSagas,
  userSagas
]

export default function* root() {
  yield all(sagas.map(saga => fork(saga)))
}