import { combineReducers } from 'redux'
import transform from 'lodash/transform'
import set from 'lodash/set'
import componentsReducers from './components/components.reducers'
import localeReducers from './locale/locale.reducers'
import userReducers from './user/user.reducers'

const all = [
  componentsReducers,
  localeReducers,
  userReducers
]

const reducers = transform(all, (reducers, value) => set(reducers, value.STATE_KEY, value.reducers), {})

export default combineReducers(reducers)