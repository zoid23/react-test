import userStorage from './user/user.storage'
import localeStorage from './locale/locale.storage'

const storages = [
  userStorage,
  localeStorage
]

export const stateFromStorage = storage => {
  const state = {}
  storages.forEach(value => {
    if (storage[value.STATE_KEY])
      state[value.STATE_KEY] = value.stateFromStorage(storage[value.STATE_KEY])
  })
  return state
}

export const storageFromState = state => {
  const storage = {}
  storages.forEach(value => {
    if (state[value.STATE_KEY])
      storage[value.STATE_KEY] = value.stateFromStorage(state[value.STATE_KEY])
  })
  return storage
}