import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import redurcers from './reducers'
import sagas from './sagas'
import createSagaMiddleware from 'redux-saga'
import { stateFromStorage, storageFromState } from './storage'

const sagaMiddleware = createSagaMiddleware()

let middleware = compose(
  applyMiddleware(sagaMiddleware)
)

if (process.env.NODE_ENV !== 'production') {
  middleware = composeWithDevTools(middleware);
}

const storage = JSON.parse(localStorage.getItem('state'))

const prevState = (storage && stateFromStorage(storage)) || {}

const store = createStore(redurcers, prevState, middleware)

store.subscribe(() => {
  const storage = storageFromState(store.getState())
  localStorage.setItem('state', JSON.stringify(storage))
})

sagaMiddleware.run(sagas)

export default store