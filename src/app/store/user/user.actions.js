import {decoreAction} from '../decorators'

export const setUser = decoreAction((email, password) => ({ 
  type: 'SET_USER',
  payload: {
    email, password
  }
}))

export const authenticateUser = decoreAction((email, password) => ({ 
  type: 'AUTHENTICATE_USER',
  payload: {
    email, password
  }
}))

export const setUserAuthenticated = decoreAction(authenticated => ({ 
  type: 'SET_USER_AUTHENTICATED',
  payload: {
    authenticated
  }
}))