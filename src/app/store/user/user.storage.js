import reducers from './user.reducers'

const stateFromStorage = storage => ({
  email: storage.email,
  password: storage.password
})

const storageFromState = state => ({
  email: state.email,
  password: state.password
})

export default {
  STATE_KEY: reducers.STATE_KEY,
  stateFromStorage,
  storageFromState
}