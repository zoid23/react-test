import get from 'lodash/get'
import reducers from './user.reducers'

export const getUser = state => get(state, `${reducers.STATE_KEY}`)