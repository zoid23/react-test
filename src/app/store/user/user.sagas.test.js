import { authenticateUser } from './user.sagas'
import { DEFAULT_STATE } from './user.reducers'
import { getUser } from './user.selectors'
import { select, put } from 'redux-saga/effects'
import * as actions from './user.actions'
import * as pwdEncrypt from '../../common/password'

jest.mock('../../common/password')
pwdEncrypt.encrypt.mockImplementation((pwd) => pwd)
pwdEncrypt.match.mockImplementation((pwd, pwdEncrypt) => pwd === pwdEncrypt)

describe('authenticateUser', () => {

  test('should register a user if not exist', () => {
    const initialState = DEFAULT_STATE

    const USER = {
      email: 'email',
      password: 'qweasd'
    }
    const gen = authenticateUser({ payload: USER })

    expect(gen.next().value).toEqual(select(getUser))

    gen.next(initialState) // timeout

    expect(gen.next().value).toEqual(
      put(actions.setUser(USER.email, pwdEncrypt.encrypt(USER.password)))
    )

    expect(gen.next().done).toBe(true)
  })

  test('should log in with correct credentials', () => {
    const USER = {
      email: 'email',
      password: 'qweasd'
    }

    const initialState = {
      ...DEFAULT_STATE,
      ...USER
    }

    const gen = authenticateUser({ payload: USER })

    expect(gen.next().value).toEqual(select(getUser))

    gen.next(initialState) // timeout

    expect(gen.next().value).toEqual(
      put(actions.setUserAuthenticated(true))
    )

    expect(gen.next().done).toBe(true)
  })

  test('should not log in with wrong credentials', () => {
    const USER = {
      email: 'email',
      password: 'qweasd'
    }

    const initialState = {
      ...DEFAULT_STATE,
      ...USER,
    }

    const gen = authenticateUser({
      payload: {
        email: 'invalid email',
        password: 'invalid password'
      }
    })

    expect(gen.next().value).toEqual(select(getUser))

    gen.next(initialState) // timeout

    expect(gen.next().value).toEqual(
      put(actions.setUserAuthenticated(false))
    )

    expect(gen.next().done).toBe(true)
  })
})