import * as actions from './user.actions'
import has from 'lodash/has'
import { getUser } from './user.selectors'
import { encrypt, match } from '../../common/password'
import { put, takeLatest, select } from 'redux-saga/effects'

export function* authenticateUser({ payload: { email, password } }) {
  const currentUser = yield select(getUser)

  yield new Promise(resolve => setTimeout(resolve, 600))

  if (!has(currentUser, 'email')) {
    yield put(actions.setUser(email, encrypt(password)))
    return
  }
  const logged = email === currentUser.email && match(password, currentUser.password)
  yield put(actions.setUserAuthenticated(logged))
}

export default function* () {
  yield takeLatest(actions.authenticateUser.type, authenticateUser);
}