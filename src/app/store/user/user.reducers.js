import { handleActions } from 'redux-actions'
import * as actions from './user.actions'

export const DEFAULT_STATE = {}

export const setUser = (state, { payload: { email, password } }) => {
  return {
    email, password,
    authenticated: true
  }
}

export const setAuthenticated = (state, { payload: { authenticated } }) => {
  return {
    ...state,
    authenticated
  }
}

const reducers = handleActions(
  {
    [actions.setUser.type]: setUser,
    [actions.setUserAuthenticated.type]: setAuthenticated
  },
  DEFAULT_STATE
)

export default {
  STATE_KEY: 'user',
  reducers
}