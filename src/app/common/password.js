import passwordHash from 'password-hash'

const SHARED_KEY = 'shared-key'

export const encrypt = (pwd) => passwordHash.generate(`${SHARED_KEY}${pwd}`)

export const match = (pwd, pwdEncripted) => passwordHash.verify(`${SHARED_KEY}${pwd}`, pwdEncripted)